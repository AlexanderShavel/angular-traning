'use strict';

angular.module("hw02App")
.directive('customModal', function(){
    return {
        templateUrl: 'views/modal.html',
        transclude: true,
        scope: {
            "header": "=header",
            "show": "=show",
            "okText": "@",
            "cancelText": "@",
            "onsubmit": "=onsubmit",
            "oncancel": "=oncancel"
        },
        link: function(scope, element, args, controller){
        }
    }
});