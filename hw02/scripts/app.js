'use strict';

/**
 * @ngdoc overview
 * @name hw01App
 * @description
 * # hw01App
 *
 * Main module of the application.
 */
angular
  .module('hw02App', ['ui.grid']);
