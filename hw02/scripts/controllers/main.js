'use strict';

/**
 * @ngdoc function
 * @name hw01App.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the hw01App
 */
angular.module('hw02App')
  .controller('MainCtrl', function ($scope, $filter) {
    $scope.gridType = 'grid';
    $scope.searchText = '';
    $scope.hwShow = false;
    $scope.buttonClass = "btn btn-default";
    $scope.phoneType = "home";
    $scope.lastViewedPerson = null;
    $scope.modal = {
      person: null,
      show: false
    };

    $scope.hwShowHide = function(){
      this.hwShow = !this.hwShow;
    };
      
    $scope.order = {field: undefined, reverse: false};
      
    $scope.orderBy = function(field){
      if ($scope.order.field === field)
        $scope.order.reverse = !$scope.order.reverse;
      else {
        $scope.order.field = field;
        $scope.order.reverse = false;
      }
    };

    $scope.showModal = function(person){
      $scope.modal.person = person;
      $scope.modal.show = true;
    };

    $scope.modalCancel = function(){
      $scope.modal.show = false;
    };

    $scope.modalSubmit = function(){
      $scope.modal.show = false;
      $scope.lastViewedPerson = $scope.modal.person;
    };


    $scope.persons = [
      {
        "firstName": "John",
        "lastName": "Smith",
        "age": 25,
        "address":
        {
          "streetAddress": "21 2nd Street",
          "city": "New York",
          "state": "NY",
          "postalCode": "10021"
        },
        "phoneNumber":
          [
            {
              "type": "home",
              "number": "212 555-1234"
            },
            {
              "type": "fax",
              "number": "646 555-4567"
            }
          ]
      },
      {
        "firstName": "Simona",
        "lastName": "Morasca",
        "age": 22,
        "address":
        {
          "streetAddress": "3 Mcauley Dr",
          "city": "Ashland",
          "state": "OH",
          "postalCode": "44805"
        },
        "phoneNumber":
          [
            {
              "type": "home",
              "number": "419-503-2484"
            },
            {
              "type": "fax",
              "number": "419-800-6759"
            }
          ]
      },
      {
        "firstName": "Josephine",
        "lastName": "Darakjy",
        "age": 33,
        "address":
        {
          "streetAddress": "4 B Blue Ridge Blvd",
          "city": "Brighton",
          "state": "MI",
          "postalCode": "48116"
        },
        "phoneNumber":
          [
            {
              "type": "home",
              "number": "973-605-6492"
            },
            {
              "type": "fax",
              "number": "602-919-4211"
            }
          ]
      }
    ];

      $scope.updateGridFilter = function(searchText) {
        if ($scope.gridType === 'grid')
          $scope.gridOptions.data = $filter('filter')($scope.persons, searchText, undefined);
      };

      $scope.gridOptions = {
        enableSorting: true,
        rowHeight:40,
        columnDefs: [
          { name: 'firstName', field: 'firstName' },
          { name: 'lastName', field: 'lastName' },
          { name: 'age', field: 'age' },
          { name: 'city', field: 'address.city' },
          {
            name: 'phone',
            field: 'phoneNumber',
            cellTemplate: '<div>{{(COL_FIELD | filter:grid.appScope.phoneType)[0].number}}</div>',
            headerCellTemplate: '<select ng-model="grid.appScope.phoneType"  class="form-control">'+
                                '<option value="home">Home Phone Number</option>'+
                                '<option value="fax">Fax Number</option>'+
                                '</select>',
            cellClass: 'cell-phone'
          },
          {
            name: 'actions',
            cellTemplate: '<button type="button" class="btn btn-default" ng-click="grid.appScope.showModal(row.entity)">'+
                          '<span class="glyphicon glyphicon-pencil"></span>'+
                          '</button>',
            cellClass:'cell-button'
          }
        ],
        data : $scope.persons
      };
    });