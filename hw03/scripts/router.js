angular.module('hw03App')
    .config(function($routeProvider){
        $routeProvider.when('/', {
            templateUrl: 'views/main.html',
            controller: 'MainCtrl'
        });
        $routeProvider.when('/list', {
            templateUrl: 'views/main.html',
            controller: 'MainCtrl'
        });
        $routeProvider.when('/person/:id', {
            templateUrl: 'views/personEdit.html',
            controller: 'PersonEdit'
        });
    $routeProvider.otherwise({
        templateUrl: 'views/404.html'
    });
});