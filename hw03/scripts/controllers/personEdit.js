'use strict';

/**
 * @ngdoc function
 * @name hw01App.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the hw01App
 */
angular.module('hw03App')
.controller('PersonEdit', function ($scope, $routeParams, $location, Persons) {
    var person = Persons.getPersonById($routeParams.id);
    $scope.person = person;
    $scope.homePhoneNumber = person.getPhoneNumber("home");
    $scope.faxNumber = person.getPhoneNumber("fax");
    
    
    $scope.save = function (form) {
        if (form.$valid) {
            person.phoneNumber = [
                {
                    "type": "home",
                    "number": $scope.homePhoneNumber
                },
                {
                    "type": "fax",
                    "number": $scope.faxNumber
                }
            ];
            person.save().then($location.path.bind($location,'/list'));
        }
    };
    
});