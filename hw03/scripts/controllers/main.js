'use strict';

/**
 * @ngdoc function
 * @name hw01App.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the hw01App
 */
angular.module('hw03App')
  .controller('MainCtrl', function ($scope, Persons) {
    $scope.persons = Persons.getPersons();
    $scope.gridType = 'table';
    $scope.searchText = '';
    $scope.buttonClass = "btn btn-default";
    $scope.phoneType = "home";
    $scope.lastViewedPerson = null;
    $scope.modal = {
      person: null,
      show: false
    };

    $scope.order = {field: undefined, reverse: false};
      
    $scope.orderBy = function(field){
      if ($scope.order.field === field)
        $scope.order.reverse = !$scope.order.reverse;
      else {
        $scope.order.field = field;
        $scope.order.reverse = false;
      }
    };

    $scope.showModal = function(person){
      $scope.modal.person = person;
      $scope.modal.show = true;
    };

    $scope.modalCancel = function(){
      $scope.modal.show = false;
    };

    $scope.modalSubmit = function(){
      $scope.modal.show = false;
      $scope.lastViewedPerson = $scope.modal.person;
    };

    $scope.updateGridFilter = function(searchText) {
      if ($scope.gridType === 'grid')
        $scope.gridOptions.data = $filter('filter')($scope.persons, searchText, undefined);
    };

    $scope.gridOptions = {
      enableSorting: true,
      rowHeight:40,
      columnDefs: [
        { 
          name: 'firstName', 
          field: 'firstName',
          cellTemplate: '<a href="#/person/{{row.entity.id}}">{{(COL_FIELD | filter:grid.appScope.firstName)}}</a>'
        },
        { name: 'lastName', field: 'lastName' },
        { name: 'age', field: 'age' },
        { name: 'city', field: 'address.city' },
        {
          name: 'phone',
          field: 'phoneNumber',
          cellTemplate: '<div>{{(COL_FIELD | filter:grid.appScope.phoneType)[0].number}}</div>',
          headerCellTemplate: '<select ng-model="grid.appScope.phoneType"  class="form-control">'+
                              '<option value="home">Home Phone Number</option>'+
                              '<option value="fax">Fax Number</option>'+
                              '</select>',
          cellClass: 'cell-phone'
        },
        {
          name: 'actions',
          cellTemplate: '<button type="button" class="btn btn-default" ng-click="grid.appScope.showModal(row.entity)">'+
                        '<span class="glyphicon glyphicon-pencil"></span>'+
                        '</button>',
          cellClass:'cell-button'
        }
      ],
      data : $scope.persons
    };
  });