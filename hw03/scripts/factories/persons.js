'use strict';

angular.module('hw03App')
    .factory('Persons', function ($q) {
        var persons = [
            {
                "id": 13457,
                "firstName": "John",
                "lastName": "Smith",
                "age": 25,
                "address": {
                    "streetAddress": "21 2nd Street",
                    "city": "New York",
                    "state": "NY",
                    "postalCode": "10021"
                },
                "phoneNumber": [
                    {
                        "type": "home",
                        "number": "212 555-1234"
                    },
                    {
                        "type": "fax",
                        "number": "646 555-4567"
                    }
                ]
            },
            {
                "id": 76578,
                "firstName": "Simona",
                "lastName": "Morasca",
                "age": 22,
                "address": {
                    "streetAddress": "3 Mcauley Dr",
                    "city": "Ashland",
                    "state": "OH",
                    "postalCode": "44805"
                },
                "phoneNumber": [
                    {
                        "type": "home",
                        "number": "419-503-2484"
                    },
                    {
                        "type": "fax",
                        "number": "419-800-6759"
                    }
                ]
            },
            {
                "id": 12583,
                "firstName": "Josephine",
                "lastName": "Darakjy",
                "age": 33,
                "address": {
                    "streetAddress": "4 B Blue Ridge Blvd",
                    "city": "Brighton",
                    "state": "MI",
                    "postalCode": "48116"
                },
                "phoneNumber": [
                    {
                        "type": "home",
                        "number": "973-605-6492"
                    },
                    {
                        "type": "fax",
                        "number": "602-919-4211"
                    }
                ]
            }
        ];
        
        var Person = function(person){
            angular.copy(person, this);
        };
        
        Person.prototype.getPhoneNumber = function(type){
            if (!this.phoneNumber)
                return null;
            var filter = this.phoneNumber.filter(function(e){return e.type == type});
            return filter.length?filter[0].number:null;
        };

        Person.prototype.save = function() {
            //not in this case but save should be a promise
            var deferred = $q.defer();
            for (var i = 0; i < persons.length; i++)
                if (persons[i].id == this.id){
                    angular.copy(this, persons[i]);
                    deferred.resolve(persons[i]);
                    var found = true;
                    break;
                }
            if (!found)
                deferred.reject("Person not found");
            return deferred.promise
        };

        return {
            getPersons: function(){
                return persons
            },

            getPersonById: function(id){
                var filter = persons.filter(function(e){ return e.id == id});
                return filter.length?new Person(filter[0]):null;
            }
        }
    });